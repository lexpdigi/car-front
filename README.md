# CarFront

Front End de Carros Angular 5+
## Scripts Disponivéis

No diretório do Projeto rodar os seguintes scripts:

### `yarn ou npm install`

Para Instalar as dependências

### `ng serve ou ng serve --port numeroDaPorta -o`

Para rodar a aplicação e abrir o app na porta desejada,
caso não tenha mudado a porta, por padrão é a 4200 
no endereço: `http://localhost:4200/`

### `ng build`

Para buildar a aplicação

### `ng test`

Para rodar os testes
