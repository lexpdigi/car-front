import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCarComponent } from './views/add-car/add-car.component';
import { ListCarComponent } from './views/list-car/list-car.component';
import { EditCarComponent } from './views/edit-car/edit-car.component';

const routes: Routes = [{
  path:'',
  component:ListCarComponent,
},
  {
    path:'cars/create',
    component:AddCarComponent
  },
  {
    path:'cars/edit/:id',
    component:EditCarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
