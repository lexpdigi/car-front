import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CarrosApiService {

  URL_API = `http://localhost:4500/`
  constructor(private http:HttpClient) { }

  getAllCars():Observable<any>{
    return this.http.get(`${this.URL_API}cars/all`)
    .pipe(map((data:any) => data.carros ))
  }

  getCarById(id:number){
    return this.http.get(`${this.URL_API}cars/find/${id}`)
    .pipe(map((data:any) => data ))
  }


  createCar(data:any){
    return this.http.post(`${this.URL_API}cars/create`,data)
    .pipe(map((resp:any) => resp ))
  }


  updateCar(id:any,data:any){
    return this.http.put(`${this.URL_API}cars/${id}`,data)
    .pipe(map((resp:any) => resp ))
  }


  deleteCar(id:number){
    return this.http.delete(`${this.URL_API}cars/${id}`,)
    .pipe(map((resp:any) => resp ))
  }
}
