 import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CarrosApiService } from '../../services/api/carros-api.service';

@Component({
  selector: 'app-list-car',
  templateUrl: './list-car.component.html',
  styleUrls: ['./list-car.component.sass']
})
export class ListCarComponent implements OnInit {

  constructor(private carsService:CarrosApiService,private router:Router) { }
  cars: any[] = [];
  displayedColumns = ['placa','marca','modelo','renavam','chassi','ano','created_at','update_at','edit','delete'];




  ngOnInit(): void {
    this.carsService.getAllCars().subscribe(
      (data)=>{
        this.cars = data;
      },
      (error)=>{

      }
    )
  }

  delete(id:number){
    this.carsService.deleteCar(id).subscribe(
      (data)=>{
       if(!data.error){
         this.cars = this.cars.filter((val) => val.id!= id)
       }
      },
      (error)=>{

      }
    )
  }

  edit(id:any){
    this.router.navigate(['cars/edit',id]);
  }

}
