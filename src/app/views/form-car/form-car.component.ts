import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CarrosApiService } from '../../services/api/carros-api.service';

@Component({
  selector: 'app-form-car',
  templateUrl: './form-car.component.html',
  styleUrls: ['./form-car.component.sass']
})
export class FormCarComponent implements OnInit {


   @Input() isEdit: boolean = false;
   private  id:number | undefined;
   private carro:any | undefined;

  constructor(private carService:CarrosApiService,private router:Router,private route:ActivatedRoute) { }

  formCar = new FormGroup({
    placa: new FormControl('',Validators.required),
    marca: new FormControl('',Validators.required),
    modelo: new FormControl('',Validators.required),
    renavam: new FormControl('',Validators.required),
    chassi: new FormControl('',Validators.required),
    ano:new FormControl('',Validators.required)
  })

  submit(){

      if(isNaN(this.formCar.value.ano)){
        alert(`Invalid Field: Ano`);
        return false;
      }

        return  (this.isEdit) ? this.updateCar():this.createCar();

  }

  ngOnInit(): void {
    if(this.isEdit){
      this.getCar();
    }
  }

  createCar(){
    return this.carService.createCar(this.formCar.value)
    .subscribe((data)=>{
      if(!data.error){
        alert('Carro Criado Com Sucesso!');
        this.router.navigate(['/'])

      }
    },(error)=>{
      console.log(error)
    });

  }

  updateCar(){
    return this.carService.updateCar(this.id,this.formCar.value)
    .subscribe((data)=>{
      if(!data.error){
        alert('Carro Atualizado Com Sucesso!');
        this.router.navigate(['/'])
      }
    },(error)=>{
      console.log(error)
     });
  }


  getCar(){
    this.id = parseInt(this.route.snapshot.params.id);

    this.carService.getCarById(this.id).subscribe(
      (data)=>{
       if(!data.error){
        this.carro = data.carro
        for(const [key,value] of Object.entries(this.carro)){
          this.formCar.get(key)?.setValue(value)
        }
       }
      },
      (error)=>{

      }
    )

  }


}
