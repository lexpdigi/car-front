import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { CarrosApiService } from '../../services/api/carros-api.service';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.sass']
})
export class AddCarComponent implements OnInit {

  ngOnInit(): void {
  }

}
